#ifndef SORTED_LIST_H
#define SORTED_LIST_H

#include <stdexcept>
#include <cassert>

namespace mtm {
    // a class for a generic sorted list
    template <typename T>
    class SortedList final {
    public:
        // a class for iterating on the list
        class const_iterator;
        /**
         * SortedList - a constructor for the list
         *
         * @return a sorted list
         */
        SortedList();
        /**
         * ~SortedList - a destructor for the list
         */
        ~SortedList();
        /**
         * SortedList - a copy constructor for the sorted list
         *
         * @param other - the sorted list to be copied
         */
        SortedList(const SortedList<T>& other);
        /**
         * operator= - a copy constructor that returns a reference to the copied sorted list
         *
         * @param other - the sorted list to be copied
         *
         * @return the reference to the sorted list
         */
        SortedList& operator=(const SortedList<T>& other);

        /**
         * insert - inserts a value of type T the sorted list
         *
         * @param value - the value to be inserted
         */
        void insert(const T& value);
        /**
         * remove - removes a value from the sorted list
         *
         * @param target - the place at which the element to be removed is
         */
        void remove(const const_iterator& target);
        // the part of the iterator pointing to the beginning of the list
        const_iterator begin() const;
        // the part of the iterator pointing to the end of the list
        const_iterator end() const;
        /**
         * @return - the length of the list
         */
        int length() const;

        /**
         * apply - applies a given function to a sorted list
         *
         * @tparam f - the function to be applied
         *
         * @return - the sorted list after having applied the function
         */
        template<typename Function>
        SortedList<T> apply(Function f) const;

        /**
         * filter - makes a list with only the elements that f returns true for them
         *
         * @param f - a function that returns true or false to check the elements
         *
         * @return - a new list with the filtered elements
         */
        template<typename Predicate>
        SortedList<T> filter(Predicate f) const;

    private:
        struct Node final {
            T value;
            Node *next;
        };

        /**
         * clear - a function to deallocate the elements of the sorted list
         */
        void clear();

        /**
         * swap - a function to switch one list with another
         *
         * @param other - the list to be switched with
         */
        void swap(SortedList<T>& other) noexcept
        {
            using std::swap;
            swap(head, other.head);
            swap(size, other.size);
        }

        Node *head = nullptr;
        int size = 0;
    };

    // constructor, already documented in declaration
    template <typename T>
    class SortedList<T>::const_iterator {
        const_iterator() = delete;
        explicit const_iterator(const Node *node);
        friend class SortedList<T>;
    public:
        ~const_iterator() = default;
        const T& operator*() const;
        bool operator==(const const_iterator& other) const;
        bool operator!=(const const_iterator& other) const;
        const_iterator& operator++();
        const_iterator operator++(int);
        const_iterator& operator=(const const_iterator& other) = default;
    private:
        const Node *current;
    };

    template <typename T>
    /**
     * operator != - checks if two sorted lists are not equal
     *
     * @param other - the sorted list to be compared with
     *
     * @return true if the lists are not the same and false otherwise
     */
    bool SortedList<T>::const_iterator::operator!=(const SortedList<T>::const_iterator& other) const
    {
        return !(*this == other);
    }

    /**
     * operator == - checks if the sorted lists are not equal
     *
     * @param other - the list to be compared with
     *
     * @return true if the lists are equal and false otherwise
     */
    template <typename T>
    bool SortedList<T>::const_iterator::operator==(const SortedList<T>::const_iterator& other) const
    {
        return current == other.current;
    }

    // the iterator for the list, already documented in declaration
    template <typename T>
    SortedList<T>::const_iterator::const_iterator(const SortedList<T>::Node *node)
        : current{node}
    {

    }

    /**
     * operator ++ - operator for advancing the list one element
     *
     * @return - the advanced sorted list
     */
    template <typename T>
    typename SortedList<T>::const_iterator& SortedList<T>::const_iterator::operator++()
    {
        if (nullptr == current) {
            throw std::out_of_range{"Iterator out of range"};
        }
        current = current->next;
        return *this;
    }

    template <typename T>
    /**
     * operator ++ - returns copy of advanced list
     *
     * @return - copy of advanced list
     */
    typename SortedList<T>::const_iterator SortedList<T>::const_iterator::operator++(int)
    {
        const const_iterator copy{*this};
        ++(*this);
        return copy;
    }

    template <typename T>
    /**
     * operator* - dereferences the iterator and returns the value it's pointing at.
     *
     * @return - the value the iterator is pointing at
     */
    const T& SortedList<T>::const_iterator::operator*() const
    {
        if (nullptr == current) {
            throw std::out_of_range{"SortedList<T>::const_iterator null dereference"};
        }
        return current->value;
    }

    // destructor
    template <typename T>
    SortedList<T>::~SortedList()
    {
        clear();
    }

    /**
     * clear - deallocates list resources
     */
    template <typename T>
    void SortedList<T>::clear()
    {
        Node *node = head;
        head = nullptr;
        while (nullptr != node) {
            const Node *to_remove = node;
            node = node->next;
            delete to_remove;
            --size;
        }
        assert(0 == size);
    }
// The rest is already documented in declarations
    template <typename T>
    void SortedList<T>::insert(const T &value)
    {
        if (nullptr == head) {
            assert(0 == size);
            head = new Node{T{value}, nullptr};
        } else {
            Node *pivot = head;
            Node *previous = nullptr;
            while ((nullptr != pivot) && (pivot->value < value)) {
                previous = pivot;
                pivot = pivot->next;
            }
            assert((nullptr == pivot) || (!(pivot->value < value)));
            assert((nullptr == previous) || (previous->value < value));
            Node *added = new Node{T{value}, pivot};
            if (nullptr == previous) {
                // assert(nullptr == head);
                head = added;
            } else {
                previous->next = added;
            }
        }
        ++size;
    }

    template <typename T>
    SortedList<T>::SortedList()
        : head{nullptr}
    {

    }

    template <typename T>
    SortedList<T>::SortedList(const SortedList<T>& other)
        : size{other.size}
    {
        if (nullptr != other.head) {
            try {
                head = new Node{T{other.head->value}, nullptr};
                Node *copy = head;
                for (Node *original = other.head->next; nullptr != original; original = original->next) {
                    Node *next = new Node{T{original->value}, nullptr};
                    copy->next = next;
                    copy = copy->next;
                }
            } catch (const std::bad_alloc &error) {
                // the temporary's destructor cleans up the partially copied list
                SortedList<T> tmp{};
                swap(tmp);
                throw;
            }
        }
    }

    template <typename T>
    SortedList<T>& SortedList<T>::operator=(const SortedList<T>& other)
    {
        SortedList copy{other};
        swap(copy);
        return *this;
    }

    template <typename T>
    int SortedList<T>::length() const
    {
        return size;
    }

    template <typename T>
    void SortedList<T>::remove(const SortedList<T>::const_iterator& target)
    {
        if (nullptr == target.current) {
            throw std::out_of_range{"SortedList<T>::remove with null target"};
        }
        assert(nullptr != target.current);
        if (target.current == head) {
            Node *to_remove = head;
            head = head->next;
            delete to_remove;
            to_remove = nullptr;
            --size;
        } else {
            assert(head->value < target.current->value);
            Node *current = head->next;
            Node *previous = head;
            while ((nullptr != current) && (current->value < target.current->value)) {
                previous = current;
                current = current->next;
            }
            assert(target.current->value <= current->value);
            if (target.current == current) {
                previous->next = current->next;
                current->next = nullptr;
                delete current;
                current = nullptr;
                --size;
            }
            // assert(target.current->value < current->value);
            assert(nullptr == current);
        }
    }

    template <typename T>
    typename SortedList<T>::const_iterator SortedList<T>::begin() const
    {
        return const_iterator{head};
    }

    template <typename T>
    typename SortedList<T>::const_iterator SortedList<T>::end() const
    {
        return const_iterator{nullptr};
    }

    template<typename T>
    template<typename Function>
    SortedList<T> SortedList<T>::apply(Function f) const
    {
        SortedList<T> result{};
        for (const_iterator iterator{begin()}; end() != iterator; ++iterator) {
            result.insert(f(*iterator));
        }
        return result;
    }

    template<typename T>
    template<typename Predicate>
    SortedList<T> SortedList<T>::filter(Predicate f) const
    {
        SortedList<T> result{};
        for (const_iterator iterator{begin()}; end() != iterator; ++iterator) {
            const T& current = *iterator;
            if (f(current)) {
                result.insert(current);
            }
        }
        return result;
    }
}

#endif // SORTED_LIST_H
