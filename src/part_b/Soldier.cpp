#include <cassert>

#include "Soldier.h"


mtm::Soldier::Soldier(const Team team, const units_t health,
                      const units_t ammo, const units_t range,
                      const units_t power)
        : Character{team, health, ammo, range, power}
{

}

mtm::Soldier *mtm::Soldier::clone() const
{
    return new Soldier{*this};
}

char mtm::Soldier::getSymbol() const
{
    return 'S';
}

mtm::units_t mtm::Soldier::getMoveRange() const
{
    return 3;
}

mtm::units_t mtm::Soldier::getReloadAmount() const
{
    return 3;
}

void mtm::Soldier::validateEnemy(const GridPoint &difference,
                                 const Character *const character) const
{
    assert((0 == norm(difference)) ^ (this != character));
    if (getRange() < norm(difference)) {
        throw OutOfRange{};
    }
    if (!hasReserveAmmo()) {
        throw OutOfAmmo{};
    }
    if ((0 != difference.row) && (0 != difference.col)) {
        throw IllegalTarget{};
    }
}

mtm::units_t mtm::Soldier::getAttackCost(const Character *const enemy)
{
    return 1;
}

mtm::units_t mtm::Soldier::getAreaOfEffectRadius() const
{
    return std::ceil(getRange() / 3.0);
}

bool mtm::Soldier::impactNearbyCharacter(units_t distance_from_target, Character &character) const
{
    if (isEnemy(character)) {
        const units_t power = getPower();
        if (0 == distance_from_target) {
            return character.hit(power);
        } else if (ceil(getRange() / 3.0) >= distance_from_target) {
            return character.hit(ceil(power / 2.0));
        }
        assert(!"Soldier::impactNearbyCharacter called with unsuitable distance from target");
    }
    return false;
}

void mtm::swap(Soldier &left, Soldier &right) noexcept
{
    using std::swap;

    swap(static_cast<Character&>(left), static_cast<Character&>(right));
}

mtm::Soldier &mtm::Soldier::operator=(const Soldier &other)
{
    Soldier copy{other};
    swap(*this, copy);
    return *this;
}
