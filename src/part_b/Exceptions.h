#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <exception>

#define DECLARE_GAME_EXCEPTION(name) \
    class name final : public Exception { \
    public: \
        virtual const char * what() const noexcept override; \
    }

namespace mtm {
    // a class for all exception types in the game
    class Exception : public std::exception {

    };
    // an exception for functions receiving arguments that do not have meaning in the context of the game.
    DECLARE_GAME_EXCEPTION(IllegalArgument);
    // an exception for a spot that does not exist on the board
    DECLARE_GAME_EXCEPTION(IllegalCell);
    // an exception for trying to interact with a character on a spot that has no character
    DECLARE_GAME_EXCEPTION(CellEmpty);
    // an exception for attempting movement that exceeds the limit of the character
    DECLARE_GAME_EXCEPTION(MoveTooFar);
    // an exception for attempting to move to an already occupied spot
    DECLARE_GAME_EXCEPTION(CellOccupied);
    // an exception for attempting to attack a spot out of range
    DECLARE_GAME_EXCEPTION(OutOfRange);
    // an exception for attempting to attack while having no ammo
    DECLARE_GAME_EXCEPTION(OutOfAmmo);
    // an exception for trying to attack a character you cannot for any other reason.
    DECLARE_GAME_EXCEPTION(IllegalTarget);
}

#endif // EXCEPTIONS_H
