#ifndef CHARACTER_H
#define CHARACTER_H

#include <iosfwd>
#include <memory>
#include <map>
#include <algorithm>

#include <cassert>

#include "Exceptions.h"
#include "Auxiliaries.h"

namespace mtm {
    /**
     * Describes an abstract character in the Powerlifters vs Crossfitters game.
     */
    class Character {
    public:
        /// As far as Character's concerned, any iterable of Cells is a valid "board"
        using Cell = const std::pair<const GridPoint, const std::shared_ptr<Character>>;

        /**
         * Creates a new Character.
         *
         * @param type the type of the character to create (must be a valid CharacterType)
         * @param team the team of the character to create (must be a valid Team)
         * @param health the health of the character to create (must be positive)
         * @param ammo the ammo of the character to create (must be non-negative)
         * @param range the range of the character to create (must be non-negative)
         * @param power the power of the character to create (must be non-negative)
         * @return a new (shared) character
         *
         * @throws IllegalArgument if any of {health, ammo, range, power} is negative
         *                          or if health is 0
         */
        Character(Team team, units_t health, units_t ammo, units_t range, units_t power);

        /**
         * Destructs a Character (owns no resources).
         */
        virtual ~Character() = default;

        /**
         * Prevent Character assignments (concrete derived Characters support assignments).
         */
        Character &operator=(const Character &other) = delete;

        /**
         * Duplicates a derived character (a virtual manual copy constructor).
         *
         * @return an independent duplicate of this character
         */
        virtual Character *clone() const = 0;

        /**
         * Formats the Character to the output stream.
         *
         * @param os the output stream to write to
         * @param character the character to format
         * @return the used output stream
         */
        friend std::ostream &operator<<(std::ostream &out, const Character &character);

        /**
         * Swaps two characters.
         */
        friend void swap(Character &left, Character &right) noexcept;


        /// Game API
        /**
         * Checks if the two characters are not on the same team.
         *
         * @param other the character to check rivalry of
         * @return true if the characters are on opposing teams, otherwise false
         */
        virtual bool isEnemy(const Character &other) const final;

        /**
         * Replenishes ammo reserves.
         */
        virtual void reload() final;

        /**
         * Makes sure a move is not out of range.
         *
         * @param move the move to validate
         * @return the validated move
         *
         * @throws MoveTooFar if the move is out of range
         */
        virtual const GridPoint &validateMove(const GridPoint &move) const final;

        /**
         * Attacks the destination.
         * Side effects: collateral damage and dead characters removal.
         *
         * Implemented as a template to decouple from the board - can be used
         * with any "board" implementation, provided it can be referred to as
         * a range of positioned characters.
         *
         * @tparam GameCharacterIterator a Cell iterator
         * @tparam HandleDead a callable that should cleanup dead characters
         *             after they die (i.e. remove them from the board) and
         *             advance the current GameCharacterIterator.
         *
         * @param position this character's position on the board
         * @param target the target's position on the board
         * @param enemy the character that's located on target on the board
         * @param cleanup the just-killed characters handler (see tparam)
         * @param begin the beginning of the board
         * @param end the end of the board
         *
         * @throws OutOfAmmo if the attacker has no available ammo reserves
         * @throws IllegalTarget if the attacker cannot attack the character at
         *                          destination (according to the game's rules)
         */
        template <typename GameCharacterIterator, typename HandleDead>
        void attack(const GridPoint &position, const GridPoint &target,
                    const std::shared_ptr<Character> enemy, const HandleDead &cleanup,
                    GameCharacterIterator begin, GameCharacterIterator end);

        // these should be protected but until C++17 it's impossible
        // (i.e. Soldier can't call protected Character::hit overrides)

        /**
         * Reduces health.
         *
         * @param damage the amount of health to remove
         * @return true if the character is now (after hitting) is dead
         */
        virtual bool hit(units_t damage) final;

        /**
         * Increases health.
         *
         * @param amount the amount of health to add
         */
        virtual void heal(units_t amount) final;

    protected:
        /// API to derived Character classes
        // (virtual final combination prevents accidental overrides)

        /// Exposes the power stat
        virtual units_t getPower() const final;

        /// Exposes the range stat
        virtual units_t getRange() const final;

        /// Checks if there's available ammo
        virtual bool hasReserveAmmo() const final;

        /// Reduces ammo (used after successful attacks)
        virtual void consumeAmmo(units_t amount) final;


        /// Requirements from derived Character classes

        /// Provides the subtype's symbol for board pretty-print
        virtual char getSymbol() const = 0;

        /// Provides the subtype's distance limit for a single move
        virtual units_t getMoveRange() const = 0;

        /// Provides the subtype's magazine size
        virtual units_t getReloadAmount() const = 0;

        /// Provides the subtype's validation of a target character
        virtual void validateEnemy(const GridPoint &difference, const Character *character) const = 0;

        /// Provides the subtype's collateral damage radius
        virtual units_t getAreaOfEffectRadius() const = 0;

        /// Called with all Characters inside the collateral damage radius around the target
        virtual bool impactNearbyCharacter(units_t distance_from_target,Character &character) const = 0;


        /// Guaranteed to be called exactly once per attack after success

        /// Provides the subtype's attack cost with respect to the target character
        virtual units_t getAttackCost(const Character *enemy) = 0;

    private:
        /// Filters collateral targets from all cells
        class InAreaOfEffect final {
        public:
            /// Constructs a predicate with respect to a character and an absolute target location
            InAreaOfEffect(const Character &character, const GridPoint &target);

            /// Destructs a predicate (owns no resources)
            ~InAreaOfEffect() = default;

            /// Prevents unsupported assignments
            InAreaOfEffect &operator=(const InAreaOfEffect &other) = delete;

            /// Determines if the Cell is inside the collateral damage radius from target
            bool operator()(Cell &pair) const;

        private:
            /// The character to use to determine the radius
            const Character * const character;

            /// The center of the collateral damage circle
            const GridPoint * const target;
        };

        /// The team the character belongs to
        Team team;

        /// The character's hit points
        units_t health;

        /// The character's ammo reserves
        units_t ammo;

        /// The character's attack range
        units_t range;

        /// The character's base damage
        units_t power;
    };

    /// Swaps two characters
    void swap(Character &left, Character &right) noexcept;

    /// A forward declaration of the output operator (see friend declaration inside Character)
    std::ostream &operator<<(std::ostream &out, const Character &character);


    /// Helper GridPoint operations

    /// Negates a coordinates pair
    GridPoint operator-(const GridPoint &coordinates);

    /// Adds a coordinates pair
    GridPoint operator+(const GridPoint &left, const GridPoint &right);

    /// Subtracts a coordinates pair
    GridPoint operator-(const GridPoint &left, const GridPoint &right);

    /// Calculates the norm of a point using the game's defined metric
    units_t norm(const GridPoint &point);


    /// Template method implementations

    template <typename GameCharacterIterator, typename HandleDead>
    void Character::attack(const GridPoint &position, const GridPoint &target,
                           const std::shared_ptr<Character> enemy, const HandleDead &cleanup,
                           GameCharacterIterator const begin, GameCharacterIterator const end)
    {
        validateEnemy(target - position, enemy.get());
        const InAreaOfEffect is_nearby{*this, target};
        GameCharacterIterator current{std::find_if(begin, end, is_nearby)};
        while (end != current) {
            const bool is_dead = impactNearbyCharacter(norm(current->first - target), *current->second);
            const GameCharacterIterator next = is_dead ? cleanup(current) : ++current;
            current = std::find_if(next, end, is_nearby);
        }
        consumeAmmo(getAttackCost(enemy.get()));
    }
}


#endif // CHARACTER_H
