#include <iostream>
#include <map>
#include <array>
#include <memory>

#include <cassert>

#include "Exceptions.h"
#include "Character.h"
#include "Soldier.h"
#include "Medic.h"
#include "Sniper.h"

#include "Game.h"

namespace mtm {
    /// Makes sure value is positive (throws mtm::IllegalArgument otherwise)
    static int validatePositive(int value);
    /// Checks if index is in the [0, size) range
    static bool isValidIndex(int index, int size);
}

mtm::Game::Game(const int height, const int width)
    : height{validatePositive(height)}
    , width{validatePositive(width)}
    , characters{}
{

}

mtm::Game::Game(const Game &other)
    : height{other.height}, width{other.width}, characters{}
{
    for (Board::const_reference pair : other.characters) {
        characters[pair.first] = std::shared_ptr<Character>{pair.second->clone()};
    }
}

mtm::Game &mtm::Game::operator=(const Game &other)
{
    Game duplicate{other};
    swap(duplicate);
    return *this;
}

void mtm::Game::addCharacter(const GridPoint &coordinates,
                             std::shared_ptr<Character> character)
{
    validateBounds(coordinates);

    if (characters.end() != characters.find(coordinates)) {
        throw CellOccupied{};
    }

    characters[coordinates] = character;
}

std::shared_ptr<mtm::Character> mtm::Game::makeCharacter(const CharacterType type,
                                                         const Team team,
                                                         const units_t health,
                                                         const units_t ammo,
                                                         const units_t range,
                                                         const units_t power)
{
    switch (type) {
        case CharacterType::SOLDIER:
            return std::make_shared<Soldier>(team, health, ammo, range, power);
        case CharacterType::MEDIC:
            return std::make_shared<Medic>(team, health, ammo, range, power);
        case CharacterType::SNIPER:
            return std::make_shared<Sniper>(team, health, ammo, range, power);
        default:
            throw IllegalArgument{};
    }
}

void mtm::Game::move(const GridPoint &src_coordinates,
                     const GridPoint &dst_coordinates)
{
    validateBounds(dst_coordinates);
    const std::shared_ptr<Character> character = find(src_coordinates);
    character->validateMove(dst_coordinates - src_coordinates);
    validateEmpty(dst_coordinates);

    characters[dst_coordinates] = character;
    characters.erase(src_coordinates);
}

void mtm::Game::attack(const GridPoint &src_coordinates,
                       const GridPoint &dst_coordinates)
{
    validateBounds(dst_coordinates);
    const std::shared_ptr<Character> character = find(src_coordinates);
    const Board::iterator target = characters.find(dst_coordinates);
    // make sure *enemy isn't destroyed during character's attack to reduce use after free chance
    const std::shared_ptr<Character> enemy =
            std::end(characters) == target ? std::shared_ptr<Character>(nullptr) : target->second;
    character->attack(src_coordinates, dst_coordinates, enemy, RemoveDead{characters},
                      std::begin(characters), std::end(characters));
}

void mtm::Game::reload(const GridPoint &coordinates)
{
    find(coordinates)->reload();
}

std::ostream &mtm::operator<<(std::ostream &out, const Game &game)
{
    const std::string delimiter = std::string(2 * game.width + 1, '*');
    out << delimiter << std::endl;
    for (int row{0}; row < game.height; ++row) {
        for (int column{0}; column < game.width; ++column) {
            const GridPoint point{row, column};
            const Game::Board::const_iterator cell{game.characters.find(point)};
            out << '|';
            if (cell == std::end(game.characters)) {
                out << ' ';
            } else {
                out << *cell->second;
            }
        }
        out << '|' << std::endl;
    }
    out << delimiter;
    return out;
}

bool mtm::Game::isOver(Team *winningTeam) const
{
    if (characters.empty()) {
        return false;
    }

    const Character *character = characters.begin()->second.get();
    assert(nullptr != character);
    const bool ongoing = std::any_of(characters.begin(), characters.end(), IsEnemyOf{*character});

    if (ongoing) {
        return false;
    }

    if (nullptr != winningTeam) {
        // TODO find a less ridiculous solution
        *winningTeam = character->isEnemy(Soldier{
            Team::POWERLIFTERS, 1, 1, 1, 1}) ? Team::CROSSFITTERS : POWERLIFTERS;
    }

    return true;
}

void mtm::Game::swap(Game &other) noexcept
{
    using std::swap;

    swap(this->height, other.height);
    swap(this->width, other.width);
    swap(this->characters, other.characters);
}

std::shared_ptr<mtm::Character> mtm::Game::find(const GridPoint &coordinates) const
{
    const Board::const_iterator iterator = characters.find(validateBounds(coordinates));
    if (std::end(characters) == iterator) {
        throw CellEmpty{};
    }
    return iterator->second;
}

const mtm::GridPoint &mtm::Game::validateBounds(const GridPoint &coordinates) const
{
    if (isValidIndex(coordinates.row, height)
        && isValidIndex(coordinates.col, width)) {
        return coordinates;
    }
    throw IllegalCell{};
}

const mtm::GridPoint &mtm::Game::validateEmpty(const GridPoint &coordinates) const
{
    if (std::end(characters) == characters.find(validateBounds(coordinates))) {
        return coordinates;
    }
    throw CellOccupied{};
}

bool mtm::operator<(const GridPoint &left, const GridPoint &right)
{
    return std::less<std::array<int, 2>>{}({left.row, left.col}, {right.row, right.col});
}

mtm::Game::IsEnemyOf::IsEnemyOf(const Character &character)
    : character{&character}
{

}

bool mtm::Game::IsEnemyOf::operator()(Board::const_reference pair) const
{
    assert(nullptr != pair.second.get());
    return character->isEnemy(*pair.second);
}

mtm::Game::RemoveDead::RemoveDead(Game::Board &characters)
        : characters{&characters}
{

}

mtm::Game::Board::iterator mtm::Game::RemoveDead::operator()(Game::Board::iterator iterator) const
{
    // make sure the character is dead
    assert(iterator->second->hit(0));
    return characters->erase(iterator);
}

static bool mtm::isValidIndex(const int index, const int size)
{
    return (0 <= index) && (index < size);
}

static int mtm::validatePositive(int value)
{
    if (0 >= value) {
        throw IllegalArgument{};
    }
    return value;
}
