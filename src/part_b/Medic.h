#ifndef MEDIC_H
#define MEDIC_H

#include "Character.h"

namespace mtm {
    /**
    * Describes a character of type medic in the game.
    */
    class Medic final : public Character {

    public:
        /**
        * Medic - a constructor for medic.
        *
        * @param team - the team to which he belongs
        * @param health - the amount of health he has
        * @param ammo - the amount of ammunition he has
        * @param range - the amount of range of attack he has
        * @param power - the amount of damage he can inflict
        *
        * @return - a medic with the provided parameters
        */
        Medic(Team team, units_t health, units_t ammo, units_t range, units_t power);
        /**
         * ~Medic - a default destructor
         */
        ~Medic() override = default;
         /**
         * operator= - a copy constructor for Medic
         *
         * @param other - the medic to be copied
         *
         * @return - the copy of the provided medic
         */
        Medic& operator=(const Medic &other);
        /**
        * clone - Duplicates a medic (a virtual manual copy constructor).
        *
        * @return an independent duplicate of this medic
        */
        virtual Medic *clone() const override final;
        /**
         * swap - swaps two medics
         *
         * @param left - one of the medics to be swapped
         * @param right - one of the medics to be swapped
         */

        friend void swap(Medic &left, Medic &right) noexcept;
        /**
         * operator - a printing operator for medic
         *
         * @param out - where the output for what is printed will be stored
         * @param medic - the medic whose parameters will be printed
         */
        friend std::ostream &operator<<(std::ostream &out, const Medic &medic);

    protected:
        /**
        * getSymbol - Provides the medics symbol for board pretty-print
        *
        * @return the medics symbol, a char.
        */
        virtual char getSymbol() const override final;
        /**
         * getMoveRange - gets the medics movement range.
         *
         * @return - returns the movement range of the medic
         */
        virtual units_t getMoveRange() const override final;
        /**
         * getReloadAmount - gets the amount of ammunition the medic can load in a single time
         *
         * @return - the amount of ammunition
         */
        virtual units_t getReloadAmount() const override final;
        /**
        * validateEnemy - checks if the medic can attack a character at that coordinate.
        *
        * @param difference - the location of the enemy.
        * @param target - the type of character the enemy is.
        *
        * @throws -
        * OutOfRange - if the target is out of range.
        * OutOfAmmo - if the medic has no ammo.
        * IllegalTarget - if he cannot attack for a different reason.
        */
        virtual void validateEnemy(const GridPoint &difference,
                                   const Character *target) const override final;
        /**
         * getAttackCost - Provides the medics attack cost with respect to the target character
         *
         * @param enemy - the enemy that was attacked
         *
         * @return - the ammunition cost of attack.
         */
        virtual units_t getAttackCost(const Character *enemy) override final;
        /**
         * getAreaOfEffectRadius - gets the radius of AOE effects.
         *
         * @return - the AOE radius
         */
        virtual units_t getAreaOfEffectRadius() const override final;
        /**
         * impactNearbyCharacter - checks if character can be impacted by attack
         *
         * @param distance_from_target - the distance from the medic
         * @param character - the impacted character
         *
         * @return - true if it can be impacted and false if not.
         */
        virtual bool impactNearbyCharacter(units_t distance_from_target,
                                           Character &character) const override final;
    };

    /**
     * swap - swaps two medics
     *
     * @param left - one of the medics to be swapped
     * @param right - one of the medics to be swapped
     */
    void swap(Medic &left, Medic &right) noexcept;
    /**
    * operator - a printing operator for medic
    *
    * @param out - where the output for what is printed will be stored
    * @param medic - the medic whose parameters will be printed
    */
    std::ostream &operator<<(std::ostream &out, const Medic &medic);
}


#endif // MEDIC_H
